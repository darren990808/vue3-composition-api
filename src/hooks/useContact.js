import { getContacts } from '../services/contacts.service'
import { computed, ref, onMounted } from 'vue';

export default function useContacts() {
    const isLoading = ref(false)
    const contacts = ref([]);
    const search = ref('');

    const filteredContacts = computed(() => {
        return contacts.value.filter(contact => {
            const fullName = `${contact.name?.first} ${contact.name?.last}`
            return fullName.toLowerCase().includes(search.value.toLowerCase())
        })
    })

    const searchContact = () => {
        search.value = query;
    }

    const fetchContacts = async () => {
        try {
            isLoading.value = true
            contacts.value = await getContacts()
        } catch (error) {
            console.log(error)
        } finally {
            isLoading.value = false
        }
    }

    onMounted(async () => {
        await fetchContacts()
    })

    return { isLoading, contacts, search, filteredContacts, searchContact, fetchContacts }
}